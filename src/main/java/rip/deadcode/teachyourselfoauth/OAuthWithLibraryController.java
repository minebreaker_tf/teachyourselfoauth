package rip.deadcode.teachyourselfoauth;

import com.google.api.client.auth.oauth2.AuthorizationCodeRequestUrl;
import com.google.api.client.auth.oauth2.AuthorizationCodeResponseUrl;
import com.google.api.client.auth.oauth2.AuthorizationCodeTokenRequest;
import com.google.api.client.auth.oauth2.ClientParametersAuthentication;
import com.google.api.client.auth.openidconnect.IdToken;
import com.google.api.client.auth.openidconnect.IdTokenVerifier;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.util.Clock;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Set;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;
import static rip.deadcode.teachyourselfoauth.Utils.gsonFactory;
import static rip.deadcode.teachyourselfoauth.Utils.httpTransport;

@Controller
public final class OAuthWithLibraryController {

    private static final String clientId = checkNotNull( System.getenv( "CLIENT_ID" ) );
    private static final String clientSecret = checkNotNull( System.getenv( "CLIENT_SECRET" ) );

    @GetMapping( "/library/login" )
    public String login( Model model ) {

        var url = new AuthorizationCodeRequestUrl( "https://accounts.google.com/o/oauth2/v2/auth", clientId )
                .setResponseTypes( Set.of( "code" ) )
                .setScopes( Set.of( "email", "profile" ) )
                .set( "access_type", "online" )
                .setState( "test_state" )
                .setRedirectUri( "http://localhost:8080/library/auth" );

        model.addAttribute( "endpoint", url );
        return "login";
    }

    @GetMapping( "/library/auth" )
    public String auth( HttpServletRequest request, Model model ) throws IOException {

        //noinspection MismatchedQueryAndUpdateOfCollection
        var responseUrl = new AuthorizationCodeResponseUrl( request.getRequestURL() + "?" + request.getQueryString() );
        checkState( responseUrl.getState().equals( "test_state" ) );

        var token = new AuthorizationCodeTokenRequest(
                httpTransport,
                gsonFactory,
                new GenericUrl( "https://www.googleapis.com/oauth2/v4/token" ),
                responseUrl.getCode()
        )
                .setClientAuthentication( new ClientParametersAuthentication( clientId, clientSecret ) )
                .setRedirectUri( "http://localhost:8080/library/auth" )
                .setGrantType( "authorization_code" )
                .execute();

        var idToken = IdToken.parse( gsonFactory, token.get( "id_token" ).toString() );
        var idTokenVerifier = new IdTokenVerifier.Builder()
                .setIssuers( Set.of( "https://accounts.google.com", "accounts.google.com" ) )
                .setAudience( Set.of( clientId ) )
                .setClock( Clock.SYSTEM )
                .build();
        checkState( idTokenVerifier.verify( idToken ) );
        var payload = idToken.getPayload();

        model.addAttribute( "email", payload.get( "email" ) )
             .addAttribute( "name", payload.get( "name" ) )
             .addAttribute( "picture", payload.get( "picture" ) )
             .addAttribute( "back", "/library/login" );

        return "result";
    }
}
