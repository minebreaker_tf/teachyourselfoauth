package rip.deadcode.teachyourselfoauth;

import com.google.common.base.Splitter;
import com.google.common.net.UrlEscapers;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.math.BigInteger;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.security.KeyFactory;
import java.security.Signature;
import java.security.spec.RSAPublicKeySpec;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;
import static rip.deadcode.teachyourselfoauth.Utils.*;

// Google Documentation
// https://developers.google.com/identity/protocols/OAuth2
// https://developers.google.com/identity/protocols/OpenIDConnect
//
// Available Scopes
// https://developers.google.com/identity/protocols/googlescopes
//
// Admin console
// https://console.developers.google.com/apis/credentials
//
// Implementation examples
// https://github.com/auth0/jwks-rsa-java/blob/master/src/main/java/com/auth0/jwk/Jwk.java
// https://github.com/auth0/java-jwt/blob/master/lib/src/main/java/com/auth0/jwt/algorithms/RSAAlgorithm.java

@Controller
public final class OAuthController {

    private static final String clientId = checkNotNull( System.getenv( "CLIENT_ID" ) );
    private static final String clientSecret = checkNotNull( System.getenv( "CLIENT_SECRET" ) );

    @GetMapping( "/login" )
    public String login( Model model ) {

        var endpoint = "https://accounts.google.com/o/oauth2/v2/auth?"
                       + "client_id=" + clientId + "&"
                       + "redirect_uri="
                       + UrlEscapers.urlFormParameterEscaper().escape( "http://localhost:8080/auth" )
                       + "&"
                       + "access_type=" + "online" + "&"
                       + "state=" + "test_state" + "&"
                       + "response_type=" + "code" + "&"
                       + "scope="
                       + UrlEscapers.urlFormParameterEscaper().escape( "email profile" );
        model.addAttribute( "endpoint", endpoint );
        return "login";
    }

    @GetMapping( "/auth" )
    public String auth( HttpServletRequest request, Model model ) throws IOException {

        var params = parseQueryParams( request );
        checkState( Objects.equals( params.get( "state" ), "test_state" ) );
        var code = URLDecoder.decode( checkNotNull( params.get( "code" ) ), StandardCharsets.UTF_8 );
        var response = httpPost(
                "https://www.googleapis.com/oauth2/v4/token",
                Map.of(
                        "code", code,
                        "client_id", clientId,
                        "client_secret", clientSecret,
                        "redirect_uri", "http://localhost:8080/auth",
                        "grant_type", "authorization_code"
                )
        ).parseAsString();
        var responseAsMap = parseJson( response );
        var id = parseIdToken( responseAsMap.get( "id_token" ) );

        model.addAttribute( "email", id.get( "email" ) )
             .addAttribute( "name", id.get( "name" ) )
             .addAttribute( "picture", id.get( "picture" ) )
             .addAttribute( "back", "/login" );

        return "result";
    }

    private static Map<String, String> parseIdToken( String idToken ) throws IOException {

        var idTokenList = Splitter.on( "." ).splitToList( idToken );

        var header = parseJson( base64ToStr( idTokenList.get( 0 ) ) );
        var body = parseJson( base64ToStr( idTokenList.get( 1 ) ) );
        var sign = idTokenList.get( 2 );

        var key = retrieveKey( header.get( "kid" ) );
        var result = verify( idTokenList.get( 0 ) + "." + idTokenList.get( 1 ), sign, key.n, key.e );
        checkState( result );
        checkState( Set.of( "https://accounts.google.com", "accounts.google.com" ).contains( body.get( "iss" ) ) );
        checkState( Objects.equals( body.get( "aud" ), clientId ) );
        var now = System.currentTimeMillis();
        checkState( now <= Long.parseLong( body.get( "exp" ) ) * 1000 + 1000 );
        checkState( now >= Long.parseLong( body.get( "iat" ) ) * 1000 - 1000 );

        return body;
    }

    private static Keys.Key retrieveKey( String kid ) throws IOException {

        var response = httpGet( "https://accounts.google.com/.well-known/openid-configuration" ).parseAsString();
        var responseAsMap = parseNestedJson( response );

        var jwks = responseAsMap.get( "jwks_uri" ).toString();
        var keyResponse = httpGet( jwks ).parseAsString();
        var keysResponseObj = parseJsonAs( keyResponse, Keys.class );
        return keysResponseObj.keys.stream()
                                   .filter( k -> k.kid.equals( kid ) )
                                   .findAny()
                                   .orElseThrow();
    }

    private static boolean verify( String contentToVerify, String sign, String n, String e ) {
        try {
            var signature = Signature.getInstance( "SHA256withRSA" );
            signature.initVerify( KeyFactory.getInstance( "RSA" ).generatePublic( new RSAPublicKeySpec(
                    new BigInteger( 1, base64ToByte( n ) ),
                    new BigInteger( 1, base64ToByte( e ) )
            ) ) );
            signature.update( contentToVerify.getBytes( StandardCharsets.UTF_8 ) );
            return signature.verify( base64ToByte( sign ) );

        } catch ( Exception ex ) {
            throw new RuntimeException( ex );
        }
    }

    @SuppressWarnings( { "unused", "MismatchedQueryAndUpdateOfCollection" } )
    private static final class Keys {

        private static final class Key {
            private String kty;
            private String alg;
            private String use;
            private String kid;
            private String n;
            private String e;
        }

        private List<Key> keys;
    }
}
